# Glassfish gfTools

These tools integrate Glassfish/Payara server with the official LetsEncrypt client [certbot](https://certbot.eff.org/)

 * leChallengeFilter - a filter that responds to LetsEncrypt Challenges
 * lePostProcessor - utility that imports a new LetsEncrypt certificate into the Glassfish keystore
 
## LE Challenge Filter 
 
There are many ways to use the certbot utility to do domain validation [User Guide](https://certbot.eff.org/docs/using.html). One option is to make the server respond to a ACME-challenge. The leChallengeFilter does exactly that; installed as a server-wide filter it'll respond to ACME-challenges. Whether or not this filter is usable in a particular case depends on your setup and use of Glassfish. If the filter doesn't do the job in your case use DNS-validation or implement a custom solution.

## LE Post Processor
The lePostProcessor is a small Java program that 
 * reads the GF master password
 * creates a backup of the old keystore
 * copies the new certificate and private key into the keystore
 * and sends an e-mail with the result
 
The master password is required to open the keystore; this requires that the master password is stored on the server. The server needs to be restarted after an update of he keystore to be able to use the new certificate. This can be done automatically via script or manually. Someone raised an issue on github some time ago ( https://github.com/payara/Payara/issues/1047 ) that contains the LetsEncrypt integration debate. The missing part so far is an asadmin-command that triggers a re-load of the SSL-certificate.

The zip-files contain source, binaries and property-files.
 
 
 
 
 
 
 
 